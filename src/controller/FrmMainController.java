/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import impl.DaoImplEmpleado;
import java.io.IOException;
import java.util.List;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import pojo.Empleado;

/**
 *
 * @author Jadpa16
 */
public class FrmMainController {

    private DaoImplEmpleado daoEmpleado;

    public FrmMainController() {
        daoEmpleado = new DaoImplEmpleado();
    }

    public DefaultTreeModel getTreeModel() throws IOException {
        List<Empleado> empleados = daoEmpleado.getAll();
        DefaultMutableTreeNode root = new DefaultMutableTreeNode("Empleados");
        
        empleados.forEach(
                (e) -> {
                    root.add(new DefaultMutableTreeNode(e));
                });
        
        return new DefaultTreeModel(root);
    }

    public void delete(Empleado e) throws IOException{
        daoEmpleado.delete(e);
    }

}
