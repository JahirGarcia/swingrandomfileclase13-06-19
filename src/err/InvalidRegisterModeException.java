/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package err;

/**
 *
 * @author Lab de Computacion
 */
public class InvalidRegisterModeException extends RuntimeException {

    private static final String MESSAGE = "Invalid register mode, use DlgEmpleado.SAVE_MODE or DlgEmpleado.EDIT_MODE";

    /**
     * Constructs an instance of <code>InvalidRegisterModeException</code> with
     * the specified detail message.
     *
     */
    public InvalidRegisterModeException() {
        super(InvalidRegisterModeException.MESSAGE);
    }
}
